# Apache Kafka training courses

## Starting Kafka in Docker
- Start Zookeeper & Kafka
```bash
docker-compose up -d
```
- Verify Zookeeper & Kafka services
```bash
docker-compose ps
```
- Verify Zookeeper is healthy
```bash
docker-compose logs zookeeper | grep -i binding

zookeeper_1  | [2019-12-31 01:56:36,468] INFO binding to port 0.0.0.0/0.0.0.0:2181 (org.apache.zookeeper.server.NIOServerCnxnFactory)
```
- Verify Kafka is healthy
```bash
docker-compose logs kafka | grep -i started

kafka_1      | [2019-12-31 01:56:40,051] INFO [SocketServer brokerId=1] Started 2 acceptor threads (kafka.network.SocketServer)
kafka_1      | [2019-12-31 01:56:40,400] INFO [ReplicaStateMachine controllerId=1] Started replica state machine with initial state -> Map() (kafka.controller.ReplicaStateMachine)
kafka_1      | [2019-12-31 01:56:40,406] INFO [PartitionStateMachine controllerId=1] Started partition state machine with initial state -> Map() (kafka.controller.PartitionStateMachine)
kafka_1      | [2019-12-31 01:56:40,439] INFO [SocketServer brokerId=1] Started processors for 2 acceptors (kafka.network.SocketServer)
kafka_1      | [2019-12-31 01:56:40,442] INFO [KafkaServer id=1] started (kafka.server.KafkaServer)
```

## Basic command
- Get into Kafka container

```bash
docker-compose exec kafka bash
```
- Create a topic
```bash
kafka-topics --create --zookeeper zookeeper:2181 \
  --replication-factor 1 --partitions 1 --topic test
```

- Create a topic with 2 partition
```
kafka-topics --create --zookeeper zookeeper:2181 \
  --replication-factor 1 --partitions 2 --topic test-2
```

- list out all kafka topic
```bash
kafka-topics --list --zookeeper zookeeper:2181
```
- describe specific kafka topic
```bash
kafka-topics --describe --zookeeper zookeeper:2181 --topic test
```
- publish data to specific kafka topic
```bash
kafka-console-producer --broker-list localhost:9092 --topic test
>Hello Kafka
>Hello World
```
- subscribe specific kafka topic
```bash
kafka-console-consumer --bootstrap-server localhost:9092 \
  --topic test --from-beginning
```

## Getting kafka consumer offsets of a consumer group

- list consumer groups
```bash
kafka-consumer-groups.sh  --list --bootstrap-server localhost:9092
```
- describe consumer group and list offset info
```bash
kafka-consumer-groups.sh --bootstrap-server localhost:9092 --describe --group group1 
```
