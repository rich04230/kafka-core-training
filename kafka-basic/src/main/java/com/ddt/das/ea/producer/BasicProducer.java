package com.ddt.das.ea.producer;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.Properties;

/**
 * @author rich
 */
@Slf4j
public class BasicProducer {
    public static void main(String[] args) {
        Properties props = new Properties();
        props.put("bootstrap.servers", "localhost:9092");
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("retries", "1");
        props.put("request.timeout.ms", "5000");

        try (Producer<String, String> producer = new KafkaProducer<>(props)) {

            String topicName = "test";
            int msgCount = 10;

            try {
                log.info("Start sending messages ...");

                // Generate Kafka records
                for (int i = 0; i < msgCount; i++) {
                    producer.send(new ProducerRecord<>(topicName, "" + i, "msg_" + i));  //fire and forget
                    log.info("Send messages to Kafka");
                }

                log.info("Send " + msgCount + " messages to Kafka");
            } catch (Exception e) {
                log.error("Unexpected error!", e);
            }
        }
        log.info("Message sending completed!");
    }
}
