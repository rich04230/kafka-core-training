package com.ddt.das.ea.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.util.Random;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SensorLog {

    private long id;
    private String temperature;


    public static SensorLog create() {
        Random r = new Random();
        String temperature = String.format("%03dF",
                r.longs(14, 120).findFirst().getAsLong());

        return new SensorLog(r.longs(1, 10).findFirst().getAsLong(),
                temperature);
    }
}
